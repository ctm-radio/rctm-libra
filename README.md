# LIBRA :libra: #

- - -

LIBRA :libra: es el nombre clave del proyecto RadioÑú para "equilibrar" la
aleatoridad de la siguientes canciónes a reproducir, para que éstas:

 * Sean menos predecibles. (:white_check_mark: LOGRADO)
 * No sean del mismo artista. (:white_check_mark: LOGRADO)
 * Sean de diferentes géneros. (:soon: PENDIENTE)
 * Sean elegidas en "bloques" de género, dependiendo de factores como la hora (:soon: PENDIENTE)
 * Consideren el sistema de peticiones (aún no anunciado) a la hora de elegir. (:soon: PENDIENTE)

LIBRA :libra: es un script de python que es llamado de forma asíncrona por
[Liquidsoap](http://liquidsoap.fm/), cada vez que este necesite de una nueva
canción para agregar a su lista de reproducción. Esto se logra gracias a la
función [`request.dynamic()`](http://liquidsoap.fm/doc-svn/request_sources.html)
que permite definir una función que se encargue de devolver una
[URI](https://es.wikipedia.org/wiki/Uniform_Resource_Identifier) que apunte a
un archivo reproducible por LS (desde archivos locales hasta otros flujos).

## Conceptos ##

Es importante destacar que LIBRA :libra: es una prueba de concepto en
desarrollo, por lo que su código puede variar notablemente entre versión y
versión. En todo caso, recomendamos leer los comentarios, para entender como
funcionan los conceptos, que se indican a continuación.

### Aleatoridad ###

La selección de la siguiente canción se realiza tomando aleatoriamente una línea
de la lista de reproducción. En cualquier método de generación de aleatoridad,
es inherente a la misma la repetición, porque no es obligación que la siguiente
selección sea distinta a la anterior. Es aquí donde entra LIBRA :libra:.

### Reducción de la repetición ###

La idea es evitar que se reproduzca una misma canción varias veces de corrido,
pero también considerando la repetición del mismo álbum o del mismo artista.
Para tal propósito, se consideran las siguientes directivas:

 * La misma canción debe reproducirse a lo más 2 veces al día (cada 12 horas).
 * El mismo álbum debe reproducirse a lo más 3 veces al día (cada 8 horas).
 * El mismo artista debe reproducirse a lo más 4 veces al día (cada 6 horas).

Con estas directivas se decide si la siguiente canción seleccionada
aleatoriamente es elegible para ser reproducida.
