#!/usr/bin/env python2
# -*- coding: utf-8 -*-

#  libra_config.py - Archivo de configuración de LIBRA
#
#  Version 0.1.0-beta
#
#  Copyleft 2014 Felipe Peñailillo Castañeda (@breadmaker in identi.ca)
#                       <breadmaker@radiognu.org>
#
#  Este programa es software libre; puede redistribuirlo y/o modificarlo bajo
#  los términos de la Licencia Pública General GNU tal como se publica por
#  la Free Software Foundation; ya sea la versión 3 de la Licencia, o
#  (a su elección) cualquier versión posterior.
#
#  Este programa se distribuye con la esperanza de que le sea útil, pero SIN
#  NINGUNA GARANTÍA; sin incluso la garantía implícita de MERCANTILIDAD o
#  IDONEIDAD PARA UN PROPÓSITO PARTICULAR. Vea la Licencia Pública
#  General de GNU para más detalles.
#
#  Debería haber recibido una copia de la Licencia Pública General de GNU
#  junto con este programa; de lo contrario escriba a la Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, EE. UU.

from os import environ

# Declaramos una variable de entorno, usada por la instrucción dataset.connect()
#       sqlite: indica que se usa una base de datos de ese motor
#       ////ruta/hacia/la/base_de_datos.db: indica la ruta absoluta donde se
#           encuentra la base de datos, tambien puede ser una ruta relativa, la
#           que debe comenzar con ///
environ["DATABASE_URL"] = 'sqlite:////ruta/hacia/la/base_de_datos.db'

# Definimos la ruta hacia el archivo que contiene el playlist. Puede ser de
# diferentes formatos, como PLS, M3U, pero en el caso de radiognu, se ha usado
# el formato de texto plano, es decir, una ruta de archivo por línea, por que
# estas rutas se guardan de esa forma en la base de datos
PLAYLIST_FILE = '/ruta/hacia/el/archivo/del/playlist.txt'
